FROM nvidia/cuda:11.8.0-devel-ubuntu22.04 as ivhd_build

WORKDIR /app

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    libboost-all-dev \
    cmake

COPY . .

RUN mkdir build && \
    cd build && \
    cmake .. && \
    make

FROM nvidia/cuda:11.8.0-runtime-ubuntu22.04

WORKDIR /app

RUN apt-get update && apt-get install -y --no-install-recommends \
    python3 \
    python3-pip

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY --from=ivhd_build /app .

ENTRYPOINT ["./run.sh"]

