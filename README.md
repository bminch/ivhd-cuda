# IVHD-CUDA

This is the *official* implementation of the **IVHD-CUDA**, which is used in "Fast visualization of large multidimensional data in GPU/CUDA environment, by embedding of sparse kNN graphs".

Contact person: Bartosz Minch (minch@agh.edu.pl), Witold Dzwinel (dzwinel@agh.edu.pl).

# Prerequisites

If you want to use application without Docker you will need:

- CUDA (8.0+), NVCC, cuBlas,
- GCC (8.0+),
- CMAKE (3.1.0+),
- FAISS,
- Python (3.6+),
 
# Installation

1. Clone this repository.
2. Init and update git submodules:
    - `git submodule init`
    - `git submodule update`

# Building

Application can be built with CMake or Docker, to build with CMake run:

`cd build && cmake .. && make`

to build with Docker run:

`sudo docker build -t <image name> .`

# Run

Command: `./run.sh [-v0|1] dataset_file labels_file knn_file results_dir knn_metric iterations nearest_neighbors random_neighbors w_random experiment_name algorithm_name seed`

- verbose mode - `-v0` - only min cost is printed, for `-v1` also cost during optimization and intermediate results are saved
- dataset_file - path to dataset in .csv format, without csv header;
- labels_file - path to text file with one label per line;
- knn_file - path of knn graph generated by Faiss, if file doesn't exist it will be generated using `faiss_generator` automatically;
- results_dir - path to directory where results should be saved;
- knn_metric - metric used to build KNN graph, must be `cosine` or `euclidean`;
- iterations - number of optimization steps;
- nearest_neighbors - number of nearest neighbors (datapoint edges in kNN graph, including self);
- random_neighbors - number of random neighbors loaded from kNN graph;
- w_random - weight of random neighbor;
- experiment_name - name of experiments;
- algorithm_name - optimization algorithms available for IVHD CUDA ("cuda_nesterov", "cuda_adadelta", "cuda_adam");
- seed - integer for deterministic rng output.

Output:
- `result_file` containing embedding output;
- `error_file` containing errors in next time steps.

Files will be saved in `results_directory` with names prefixed with `experiment_name`.

Alternatively, using Docker:

```
sudo docker run --gpus=all -it \
    -v <directory where your dataset and labels are saved>:/app/data \
    -v <directory where you want results to be saved>:/app/results \ 
    <image name> <verbose_mode> /app/data/<dataset_file_name> /app/data/<labels_file_name> /app/data/<knn_graph_file_name> \
    /app/results <knn_metric> <iterations> <nearest_neighbors> <random_neighbors> <w_random> <experiment_name> <algorithm_name> <seed>
```

# Visualization

- Calculating kNN metric used in the paper attached above:
  - `python ./knn_metric.py result_file number_of_neighbors`

- Plotting error:
  - `python ./plot_error.py error_file`

- Drawing results on 2-D plane:
  - `python ./draw.py result_file`