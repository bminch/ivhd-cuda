#include <cuda.h>
#include <algorithm>
#include <cassert>
#include <cstring>
#include <iostream>
#include <unordered_map>
#include <thrust/execution_policy.h>
#include <thrust/device_ptr.h>
#include <thrust/reduce.h>
#include "constants.h"
#include "caster/caster_cuda.h"

using namespace std;

__global__ void initializeSamples(int n, Sample *samples, float2 *positions)
{
    unsigned i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n)
    {
        Sample sample{};
        sample.pos = positions[i];
        sample.v = {0, 0};
        sample.aggregated_force = {0, 0};
        samples[i] = sample;
    }
}

void CasterCuda::initializeHelperVectors()
{
    int threadsInBlock = 128;
    int numBlocks = (positions.size() + threadsInBlock - 1) / threadsInBlock;
    initializeSamples<<<numBlocks, threadsInBlock>>>(positions.size(), d_samples, d_positions);
}

/*
 * This function performs the preprocessing on the CPU that is optional
 *
 * Sorts samples by number of their distances and sorts distances by
 * index i or j to utilize cache better. After sorting samples, their indexes
 * change so we have to update distances once more
 */
void CasterCuda::sortHostSamples(vector<int> &labels)
{
    // create array of sorted indexes
    vector<short> sampleFreq(positions.size());
    for (unsigned i = 0; i < positions.size(); i++)
    {
        sampleFreq[i] = 0;
    }

    vector<int> sampleIndexes(positions.size());
    for (unsigned i = 0; i < positions.size(); i++)
    {
        sampleIndexes[i] = i;
    }

    sort(sampleIndexes.begin(), sampleIndexes.end(),
        [&sampleFreq](const int &a, const int &b) -> bool
        {
        if (sampleFreq[a] != sampleFreq[b])
        {
            return sampleFreq[a] < sampleFreq[b];
        }
        else
        {
            return a < b;
        }
    });

    // create mapping index->new index
    vector<int> newIndexes(positions.size());
    for (unsigned i = 0; i < positions.size(); i++)
    {
        newIndexes[sampleIndexes[i]] = i;
    }

    // sort positions
    vector<float2> positionsCopy = positions;
    vector<int> labelsCopy = labels;
    for (unsigned i = 0; i < positions.size(); i++)
    {
        positions[i] = positionsCopy[sampleIndexes[i]];
        labels[i] = labelsCopy[sampleIndexes[i]];
    }

    // update indexes in distances
    for (auto & distance : distances)
    {
        distance.i = newIndexes[distance.i];
        distance.j = newIndexes[distance.j];
    }

    // sort distances
    sort(distances.begin(), distances.end(),[](const DistElem &a, const DistElem &b) -> bool
    {
        if (a.i != b.i)
        {
            return a.i < b.i;
        }
        else
        {
            return a.j <= b.j;
        }
    });
}

bool CasterCuda::allocateInitializeDeviceMemory()
{
    cuCall(cudaMalloc(&d_positions, positions.size() * sizeof(float2)));
    cuCall(cudaMalloc(&d_samples, positions.size() * sizeof(Sample)));
    cuCall(cudaMalloc(&d_distances, distances.size() * sizeof(DistElem)));
    cuCall(cudaMalloc(&d_errors, distances.size() * sizeof(float)));

    cuCall(cudaMemcpy(d_positions, positions.data(),
                sizeof(float2) * positions.size(), cudaMemcpyHostToDevice));
    cuCall(cudaMemset(d_samples, 0, positions.size() * sizeof(Sample)));
    cuCall(cudaMemset(d_errors, 0, distances.size() * sizeof(float)));
    cuCall(cudaMemcpy(d_distances, distances.data(),
                sizeof(DistElem) * distances.size(),
                cudaMemcpyHostToDevice));

    return true;
}

void CasterCuda::prepare(vector<int> &labels, vector<float2> initialPositions)
{
    positions = initialPositions;
    sortHostSamples(labels);
    allocateInitializeDeviceMemory();
    initializeHelperVectors();
}

std::vector<float2> CasterCuda::finish()
{
    std::vector<float2> results = copyPositions();
    cuCall(cudaFree(d_positions));
    cuCall(cudaFree(d_distances));
    cuCall(cudaFree(d_samples));

    return results;
}

__global__ void copyDevicePos(int N, Sample *samples, float2 *positions)
{
    unsigned i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < N)
    {
        positions[i] = samples[i].pos;
    }
}

std::vector<float2>& CasterCuda::copyPositions()
{
    copyDevicePos<<<positions.size() / 256 + 1, 256>>>(positions.size(),
            d_samples, d_positions);
    cuCall(cudaMemcpy(positions.data(), d_positions,
                sizeof(float2) * positions.size(), cudaMemcpyDeviceToHost));
    return positions;
}

__global__ void calculateErrors(int dstNum, DistElem *distances, Sample *samples, float *errors)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if(i < dstNum)
    {
        DistElem dist = distances[i];
        float d = dist.r;
        float2 iPos = samples[dist.i].pos;
        float2 jPos = samples[dist.j].pos;
        float2 ij = {iPos.x - jPos.x, jPos.y - jPos.y};
        errors[i] = fabs(d - sqrtf(ij.x * ij.x + ij.y * ij.y));
    }
}

float CasterCuda::getError()
{
    int threadsInBlock = 1024;
    int numBlocks = (distances.size() + threadsInBlock - 1) / threadsInBlock;
    calculateErrors<<<numBlocks, threadsInBlock>>>(distances.size(), d_distances, d_samples, d_errors);

    thrust::device_ptr<float> err_ptr = thrust::device_pointer_cast(d_errors);
    return thrust::reduce(err_ptr, err_ptr + distances.size(), 0.0, thrust::plus<float>());
}

void CasterCuda::simul_step()
{
    step++;
    simul_step_cuda();
};
