#include <cuda.h>
#include "constants.h"
#include "caster/caster_cuda_adadelta.h"
using namespace std;

#define DECAYING_PARAM 0.1f

__global__ void calcPositionsAdadelta(long n, Sample *samples, float4 *avarage_params)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if(i < n)
    {
        Sample sample = samples[i];
        float4 avarage_param = avarage_params[i];

        avarage_param.x = avarage_param.x * DECAYING_PARAM + (1.0f - DECAYING_PARAM) * sample.aggregated_force.x * sample.aggregated_force.x;
        avarage_param.y = avarage_param.y * DECAYING_PARAM + (1.0f - DECAYING_PARAM) * sample.aggregated_force.y * sample.aggregated_force.y;

        float deltax = sample.aggregated_force.x / sqrtf(EPS + avarage_param.x) * sqrtf(EPS + avarage_param.z);
        float deltay = sample.aggregated_force.y / sqrtf(EPS + avarage_param.y) * sqrtf(EPS + avarage_param.w);

        sample.pos.x += deltax;
        sample.pos.y += deltay;

        avarage_param.z = avarage_param.z * DECAYING_PARAM + (1.0f - DECAYING_PARAM) * deltax * deltax;
        avarage_param.w = avarage_param.w * DECAYING_PARAM + (1.0f - DECAYING_PARAM) * deltay * deltay;

        sample.aggregated_force = {0, 0};
        samples[i] = sample;
        avarage_params[i] = avarage_param;
    }
}

__global__ void calcForceComponentsAdadelta(int compNumber, DistElem *distances,
        Sample *samples, float w_random)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if(i < compNumber)
    {
        DistElem distance = distances[i];

        float2 posI = samples[distance.i].pos;
        float2 posJ = samples[distance.j].pos;

        float2 rv = posI;
        rv.x -= posJ.x;
        rv.y -= posJ.y;

        float point_dist = (posI.x - posJ.x) * (posI.x - posJ.x) + (posI.y - posJ.y) * (posI.y - posJ.y);
        float r = point_dist < EPS ? EPS : sqrtf(point_dist);
        float D = distance.r;

        float energy = (r - D) / r;
        rv.x *= -energy;
        rv.y *= -energy;

        // distances are sorted by their type
        if (distance.type == etRandom)
        {
            rv.x *= w_random;
            rv.y *= w_random;
        }

        atomicAdd(&samples[distance.i].aggregated_force.x, rv.x);
        atomicAdd(&samples[distance.i].aggregated_force.y, rv.y);
        atomicAdd(&samples[distance.j].aggregated_force.x, -rv.x);
        atomicAdd(&samples[distance.j].aggregated_force.y, -rv.y);
    }
}

void CasterCudaAdadelta::simul_step_cuda()
{
    int forceThreads = 128;
    int forceNumBlocks = (distances.size() + forceThreads - 1) / forceThreads;

    int positionsThreads = 256;
    int positionsNumBlocks = (positions.size() + positionsThreads - 1) / positionsThreads;

    calcForceComponentsAdadelta<<<forceNumBlocks, forceThreads>>>(distances.size(), d_distances, d_samples, w_random);
    calcPositionsAdadelta<<<positionsNumBlocks, positionsThreads>>>(positions.size(), d_samples, d_average_params);
}

void CasterCudaAdadelta::prepare(vector<int> &labels, vector<float2> initialPositions)
{
    CasterCuda::prepare(labels, initialPositions);

    cuCall(cudaMalloc(&d_average_params, positions.size() * sizeof(float4)));
    cuCall(cudaMemset(d_average_params, 0, positions.size() * sizeof(float4)));
}


vector<float2> CasterCudaAdadelta::finish()
{
    cuCall(cudaFree(d_average_params));
    return CasterCuda::finish();
}

