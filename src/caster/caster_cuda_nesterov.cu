#include <cuda.h>
#include "constants.h"
#include "caster/caster_cuda_nesterov.h"
using namespace std;

__global__ void calcPositionsNesterov(long n, Sample *samples) 
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n)
    {
        Sample sample = samples[i];

        sample.v.x = sample.v.x * a_factor + sample.aggregated_force.x * b_factor;
        sample.v.y = sample.v.y * a_factor + sample.aggregated_force.y * b_factor;

        sample.aggregated_force = {0, 0};

        sample.pos.x += sample.v.x;
        sample.pos.y += sample.v.y;

        samples[i] = sample;
    }
}

__global__ void calcForceComponentsNesterov(int compNumber, DistElem *distances,
        Sample *samples, float w_random)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < compNumber) {
        DistElem distance = distances[i];

        float2 posI = samples[distance.i].pos;
        float2 vI = samples[distance.i].v;
        float2 posJ = samples[distance.j].pos;
        float2 vJ = samples[distance.j].v;

        // approximate next positions
        posI.x += vI.x;
        posI.y += vI.y;
        posJ.x += vJ.x;
        posJ.y += vJ.y;

        float2 rv = posI;
        rv.x -= posJ.x;
        rv.y -= posJ.y;

        float point_dist = (posI.x - posJ.x) * (posI.x - posJ.x) + (posI.y - posJ.y) * (posI.y - posJ.y);
        float r = point_dist < EPS ? EPS : sqrtf(point_dist);
        float D = distance.r;

        float energy = (r - D) / r;
        rv.x *= -energy;
        rv.y *= -energy;

        // distances are sorted by their type
        if (distance.type == etRandom) {
            rv.x *= w_random;
            rv.y *= w_random;
        }

        atomicAdd(&samples[distance.i].aggregated_force.x, rv.x);
        atomicAdd(&samples[distance.i].aggregated_force.y, rv.y);
        atomicAdd(&samples[distance.j].aggregated_force.x, -rv.x);
        atomicAdd(&samples[distance.j].aggregated_force.y, -rv.y);
    }
}



void CasterCudaNesterov::simul_step_cuda()
{
    int forceThreads = 128;
    int forceNumBlocks = (distances.size() + forceThreads - 1) / forceThreads;

    int positionsThreads = 256;
    int positionsNumBlocks = (positions.size() + positionsThreads - 1) / positionsThreads;

    calcForceComponentsNesterov<<<forceNumBlocks, forceThreads>>>(distances.size(), d_distances, d_samples, w_random);
    calcPositionsNesterov<<<positionsNumBlocks, positionsThreads>>>(positions.size(), d_samples);
}
