#include <cuda.h>
#include "constants.h"
#include "caster/caster_cuda_adam.h"
using namespace std;

#define B1 0.9f
#define B2 0.999f
#define LEARNING_RATE 0.002f

__global__ void calcPositionsAdam(long n, unsigned it, Sample *samples, float4 *average_params)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if (i < n)
    {
        Sample sample = samples[i];
        float4 average_param = average_params[i];

        average_param.x = average_param.x * B2 + (1.0f - powf(B2, static_cast<float>(it))) * sample.aggregated_force.x * sample.aggregated_force.x;
        average_param.y = average_param.y * B2 + (1.0f - powf(B2, static_cast<float>(it))) * sample.aggregated_force.y * sample.aggregated_force.y;

        average_param.z = average_param.z * B1 + (1.0f - powf(B1, static_cast<float>(it))) * sample.aggregated_force.x;
        average_param.w = average_param.w * B1 + (1.0f - powf(B1, static_cast<float>(it))) * sample.aggregated_force.y;

        float deltax = LEARNING_RATE * (average_param.z / (1.0f - B1)) / (EPS + sqrtf(average_param.x / (1.0f - B2)));
        float deltay = LEARNING_RATE * (average_param.w / (1.0f - B1)) / (EPS + sqrtf(average_param.y / (1.0f - B2)));

        sample.pos.x += deltax;
        sample.pos.y += deltay;

        sample.aggregated_force = {0, 0};
        samples[i] = sample;
        average_params[i] = average_param;
    }
}

__global__ void calcForceComponentsAdam(int compNumber, DistElem *distances,
        Sample *samples, float w_random)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    if(i < compNumber) 
    {
        DistElem distance = distances[i];

        float2 posI = samples[distance.i].pos;
        float2 posJ = samples[distance.j].pos;

        float2 rv = posI;
        rv.x -= posJ.x;
        rv.y -= posJ.y;

        float point_dist = (posI.x - posJ.x) * (posI.x - posJ.x) + (posI.y - posJ.y) * (posI.y - posJ.y);
        float r = point_dist < EPS ? EPS : sqrtf(point_dist);
        float D = distance.r;

        float energy = (r - D) / r;
        rv.x *= -energy;
        rv.y *= -energy;

        // distances are sorted by their type
        if (distance.type == etRandom)
        {
            rv.x *= w_random;
            rv.y *= w_random;
        }

        atomicAdd(&samples[distance.i].aggregated_force.x, rv.x);
        atomicAdd(&samples[distance.i].aggregated_force.y, rv.y);
        atomicAdd(&samples[distance.j].aggregated_force.x, -rv.x);
        atomicAdd(&samples[distance.j].aggregated_force.y, -rv.y);
    }
}

void CasterCudaAdam::simul_step_cuda()
{
    int forceThreads = 128;
    int forceNumBlocks = (distances.size() + forceThreads - 1) / forceThreads;

    int positionsThreads = 256;
    int positionsNumBlocks = (positions.size() + positionsThreads - 1) / positionsThreads;

    calcForceComponentsAdam<<<forceNumBlocks, forceThreads>>>(distances.size(), d_distances, d_samples, w_random);
    calcPositionsAdam<<<positionsNumBlocks, positionsThreads>>>(positions.size(), step, d_samples, d_average_params);
}

void CasterCudaAdam::prepare(vector<int> &labels, vector<float2> initialPositions)
{
    CasterCuda::prepare(labels, initialPositions);

    cuCall(cudaMalloc(&d_average_params, positions.size() * sizeof(float4)));
    cuCall(cudaMemset(d_average_params, 0, positions.size() * sizeof(float4)));
}

vector<float2> CasterCudaAdam::finish()
{
    cuCall(cudaFree(d_average_params));
    return CasterCuda::finish();
}
