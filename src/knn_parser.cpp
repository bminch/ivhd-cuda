#include <boost/algorithm/string.hpp>
#include <cassert>
#include <fstream>
#include <iostream>
#include <vector>
#include "knn_parser.h"
#include "distance.h"

void KNNParser::parseFile(const std::string& fileName,
                          const std::function<void(int, int)>& handlePair)
{
    std::ifstream input_file(fileName);

    // read header
    long firstLineMaxSize = 64;
    char firstLine[64];
    input_file.getline(firstLine, firstLineMaxSize);

    std::vector<std::string> splits;
    boost::split(splits, firstLine, [](char c) { return c == ';'; });

    assert(splits.size() == 3);

    int graphSize = stoi(splits[0]);
    int nearestNeighborsCount = stoi(splits[1]);
    int longSize = stoi(splits[2]);

    auto testNum = 0;
    input_file.read(reinterpret_cast<char*>(&testNum), longSize);
    assert(testNum == 0x01020304);

    // parse neighbours
    for (int i = 0; i < graphSize; i++) {
        for (int j = 0; j < nearestNeighborsCount; j++) {
            long neighbor;
            float distance;
            input_file.read(reinterpret_cast<char*>(&neighbor), longSize);
            input_file.read(reinterpret_cast<char*>(&distance), longSize / 2);
            assert(input_file.gcount() == 8 || input_file.gcount() == longSize / 2);

            handlePair(i, neighbor);
        }
    }

    input_file.close();
}
