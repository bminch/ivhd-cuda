#include <random>
#include "data.h"
#include "config.h"
#include "engine.h"

int main(int argc, char* argv[])
{
    Config config(argc, argv);
    srand(config.seed);

    Data data(&config);
    data.load();

    Engine engine(&data, &config);
    engine.run();

    return 0;
}
