#include <iostream>
#include "config.h"

using namespace std;

Config::Config(int argc, char** argv)
{   
    if (argc != 12)
    {
        cerr << "Expected 11 arguments:\n";
        cerr << "./ivhd_cuda [-v0|1] dataset_file labels_file knn_file results_dir iterations random_neighbours"
                " w_random experiment_name algorithm_name seed\n";
        exit(-1);
    }
    
    verbose = string(argv[1]) == "-v1";
    dataset_file = argv[2];
    labels_file = argv[3];
    knn_file = argv[4];
    results_dir = argv[5];
    if (results_dir[results_dir.size() - 1] == '/')
    {
        results_dir.pop_back();
    }
    iterations = stoi(argv[6]);
    random_neighbours = stoi(argv[7]);
    w_random = stof(argv[8]);
    experiment_name = argv[9];
    caster_type = parseCasterType(argv[10]);
    seed = stoi(argv[11]);
}

CasterType Config::parseCasterType(string algorithm_name)
{    
    if (algorithm_name == "cuda_ab")
    {
        return AB;
    }
    else if (algorithm_name == "cuda_nesterov")
    {
        return NESTEROV;
    }
    else if (algorithm_name == "cuda_adadelta")
    {
        return ADADELTA;
    }
    else if (algorithm_name == "cuda_adam")
    {
        return ADAM;

    }
    else
    {
        cerr << "Invalid algorithm_name. Expected one of: 'cuda_ab', ";
        cerr << "'cuda_nesterov', 'cuda_adadelta', 'cuda_adam'\n";
        exit(-1);
    }    
}
