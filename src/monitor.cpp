#include "monitor.h"

using namespace std::chrono;
using namespace std;


DummyMonitor::DummyMonitor() {
    minError = numeric_limits<float>::max();
}

void DummyMonitor::recordError(float err) {
    minError = min(minError, err);
}

void DummyMonitor::printSummary() {
    cerr << "minError: " << minError << endl;
}

PerformanceMonitor::PerformanceMonitor(const vector<int>* labels, Config* config)
{
    this->labels = labels;
    this->config = config;
    minError = numeric_limits<float>::max();
    offset = 0;

    errFile.open(config->results_dir + "/" + config->experiment_name + "_error");
}

void PerformanceMonitor::startTimer()
{
    system_clock::time_point now = system_clock::now();
    start = time_point_cast<milliseconds>(now).time_since_epoch().count();
}

void PerformanceMonitor::recordError(float err)
{
    system_clock::time_point now = system_clock::now();
    minError = min(minError, err);
    auto time = time_point_cast<milliseconds>(now).time_since_epoch().count() -
                            start - offset;

    errFile << time << " " << err << endl;
}

void PerformanceMonitor::recordPositions(vector<float2>& positions)
{
    system_clock::time_point now = system_clock::now();
    auto time = time_point_cast<milliseconds>(now).time_since_epoch().count() -
                            start - offset;

    ofstream posFile;
    posFile.open(config->results_dir + "/" + config->experiment_name + "_" + to_string(time) + "_positions");

    for (unsigned i = 0; i < positions.size(); i++) {
        posFile << positions[i].x << " " << positions[i].y << " "
                        << (*labels)[i] << endl;
    }
    posFile.close();

    system_clock::time_point end = system_clock::now();
    offset += time_point_cast<milliseconds>(end).time_since_epoch().count() -
                        time_point_cast<milliseconds>(now).time_since_epoch().count();
}

void PerformanceMonitor::printSummary()
{
    system_clock::time_point now = system_clock::now();
    auto totalTime =
            time_point_cast<milliseconds>(now).time_since_epoch().count() - start -
            offset;
    cerr << totalTime << endl;
    cerr << "minError: " << minError << endl;
}

PerformanceMonitor::~PerformanceMonitor()
{
    errFile.close();
}
