#include "data.h"
#include <cstdlib>
#include "csv-parser/parser.hpp"
#include "knn_parser.h"

using namespace std;

void Data::load()
{
    ifstream datasetFile(config->dataset_file);
    ifstream labelsFile(config->labels_file);

    datasetFile.clear();
    datasetFile.seekg(0);
    labelsFile.clear();
    labelsFile.seekg(0);
    aria::csv::CsvParser datasetParser(datasetFile);
    aria::csv::CsvParser labelsParser(labelsFile);

    for (auto& row : labelsParser)
    {
        for (const string& field : row)
        {
            labels.push_back(stoi(field));
            particleCount++;
        }
    }

    data.resize(particleCount);

    int ind = 0;
    int parserIt = 0;
    for (auto& row : datasetParser)
    {
        if (parserIt++ <= 1)
        {
            continue;
        }

        for (const string& field : row)
        {
            data[ind].push_back(stoi(field));
        }
        ind++;
    }

    datasetFile.close();
    labelsFile.close();
}

void Data::generateRandomDistances(IDistanceContainer& dstContainer, int n, unsigned rn)
{
    for (int i = 0; i < n; i++)
    {
        unsigned rn_it = rn;
        while (rn_it--) {
            int randIndex = rand() % n;
            while (randIndex == i && !dstContainer.containsDst(i, randIndex))
            {
                randIndex = rand() % n;
            }

            DistElem distElem(i, randIndex, DistElemType::etRandom, 1);
            dstContainer.addDistance(distElem);
        }
    }
}

void Data::generateNearestDistances(IDistanceContainer& dstContainer, const string& file)
{
        auto lambda = [&dstContainer](int x, int y)
        {
                DistElem distElem(x, y);
                dstContainer.addDistance(distElem);
        };

        KNNParser::parseFile(file, lambda);
}
