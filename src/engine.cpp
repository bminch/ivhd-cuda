#include "engine.h"
#include "caster/caster_cuda_ab.h"
#include "caster/caster_cuda_adadelta.h"
#include "caster/caster_cuda_adam.h"
#include "caster/caster_cuda_nesterov.h"

Engine::Engine(Data* data, Config* config)
{
    this->data = data;
    this->config = config;
    this->monitor = config->verbose ?
        (Monitor*) new PerformanceMonitor(&data->labels, config) : (Monitor*) new DummyMonitor;

    setCaster();
    cudaDeviceSetLimit(cudaLimitMallocHeapSize, HEAP_LIMIT);
}

Engine::~Engine()
{
    if (caster != nullptr)
    {
        delete caster;
    }
    delete monitor;
}

void Engine::setCaster()
{
    switch(config->caster_type)
    {
        case AB: 
            caster = new CasterCudaAB(config->w_random);
            break;
        case NESTEROV: 
            caster = new CasterCudaNesterov(config->w_random);
            break;
        case ADADELTA: 
            caster = new CasterCudaAdadelta(config->w_random);
            break;
        case ADAM:
            caster = new CasterCudaAdam(config->w_random);
            break;
    }
}

void Engine::prepareDataset()
{
    Data::generateNearestDistances(*caster, config->knn_file);
    Data::generateRandomDistances(*caster, data->size(), config->random_neighbours);

    std::random_device rd;
    std::mt19937 gen(rd());
    gen.seed(config->seed);
    std::uniform_real_distribution<> dis(0.0f, 1.0f);

    vector<float2> initialPositions(data->size());
    for (int i = 0; i < data->size(); i++)
    {
        initialPositions[i].x = dis(gen);
        initialPositions[i].y = dis(gen);
    }

    caster->prepare(data->labelsRef(), initialPositions);
}

void Engine::run()
{
    monitor->startTimer();

    prepareDataset();

    cudaDeviceSynchronize();

    optimize();

    caster->finalize();

    cudaDeviceSynchronize();

    std::vector<float2> results = caster->finish();

    monitor->printSummary();
    saveResults(results);
}

void Engine::optimize()
{

    for (unsigned i = 0; i < config->iterations; i++)
    {
        caster->simul_step();

        if((i + 1) % config->record_error_iterations_period == 0)
        {
            monitor->recordError(caster->getError());
        }

        if((i + 1) % config->record_positions_iterations_period == 0)
        {
            cudaDeviceSynchronize();
            monitor->recordPositions(caster->copyPositions());
        }
    }
}


void Engine::saveResults(std::vector<float2>& positions)
{
    ofstream results;
    results.open(config->results_dir + "/" + config->experiment_name + "_result");
    for (int i = 0; i < data->size(); i++)
    {
        results << positions[i].x << " " << positions[i].y << " "
                        << data->labels[i] << endl;
    }

    results.close();
}
