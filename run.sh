#!/bin/bash

if [ "$#" -ne 13 ]; then
    echo "Expected 13 arguments: [-v0|1] dataset_file labels_file knn_file results_dir \
          knn_metric iterations nearest_neighbors random_neighbors w_random experiment_name algorithm_name seed"
    exit 1
fi

IVHD_CUDA=./build/src/ivhd_cuda

VERBOSE=$1
DATASET_FILE=$2
LABELS_FILE=$3
KNN_FILE=$4
RESULTS_DIR=$5

KNN_METRIC=$6
ITERATIONS=$7
NEAREST_NEIGHBORS=$8
RANDOM_NEIGHBORS=$9
W_RANDOM="${10}"
EXPERIMENT_NAME="${11}"
ALGORITHM_NAME="${12}"
SEED="${13}"

if [[ ! -f "$KNN_FILE" ]]; then
    echo "Generating KNN graph..."
    python3 faiss_generator.py $NEAREST_NEIGHBORS $KNN_METRIC $DATASET_FILE $KNN_FILE
else
    echo "KNN graph already present, skipping generation"
fi

echo "Running ivhd..."

$IVHD_CUDA $VERBOSE $DATASET_FILE $LABELS_FILE $KNN_FILE $RESULTS_DIR $ITERATIONS $RANDOM_NEIGHBORS $W_RANDOM $EXPERIMENT_NAME $ALGORITHM_NAME $SEED

echo "Saved results to $RESULTS_DIR"
