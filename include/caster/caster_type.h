#pragma once 

enum CasterType {
    NESTEROV,
    AB,
    ADADELTA,
    ADAM
};
