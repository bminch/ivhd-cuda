#pragma once

#include <utility>
#include "caster/caster_cuda.h"

class CasterCudaAdam : public CasterCuda {
public:
    CasterCudaAdam(float w_random): CasterCuda(w_random) {}

    void prepare(vector<int> &labels, std::vector<float2> initialPositions) override;
    std::vector<float2> finish() override;

protected:
    void simul_step_cuda() override;

private:
    float4 *d_average_params{};
};
