#pragma once

#include <utility>
#include "caster/caster_cuda.h"

class CasterCudaNesterov : public CasterCuda
{
public:
    CasterCudaNesterov(float w_random): CasterCuda(w_random) {}

protected:
    void simul_step_cuda() override;
};
