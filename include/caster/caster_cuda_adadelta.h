#pragma once

#include <utility>
#include "caster/caster_cuda.h"

class CasterCudaAdadelta : public CasterCuda {
public:
    CasterCudaAdadelta(float w_random): CasterCuda(w_random) {}

    void prepare(std::vector<int> &labels, std::vector<float2> initialPositions) override;
    std::vector<float2> finish() override;

protected:
    void simul_step_cuda() override;

private:
// x,y -> gradient, z,w -> param
    float4 *d_average_params{};
};
