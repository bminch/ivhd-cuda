#pragma once

#include <cmath>
#include <functional>
#include <utility>
#include <vector>
#include "distance.h"
#include "distance_container.h"

class Caster : public IDistanceContainer
{
public:
    Caster() {};

    virtual void simul_step() = 0;
    virtual void prepare(std::vector<int>& labels, std::vector<float2> initialPositions){};
    virtual void finalize(){};
    virtual std::vector<float2> finish() = 0;
    virtual float getError() = 0;
    virtual std::vector<float2>& copyPositions() = 0;

protected:
    std::vector<float2> positions;
};
