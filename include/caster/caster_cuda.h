#pragma once

#include <utility>
#include <vector>
#include "caster/caster.h"
#include "distance.h"
#include "distance_container.h"

using namespace std;

struct Sample
{
    float2 pos;
    float2 v;
    float2 aggregated_force;
};

class CasterCuda : public Caster
{
    public:
        CasterCuda(float w_random): w_random(w_random) {}

        void sortHostSamples(vector<int> &labels);

        float2 *d_positions{};
        DistElem *d_distances{};
        Sample *d_samples{};
        float *d_errors{};

        void prepare(vector<int> &labels, vector<float2> initialPositions) override;
        std::vector<float2> finish() override;
        void simul_step() override;
        float getError() override;
        std::vector<float2>& copyPositions() override;
    
        bool allocateInitializeDeviceMemory();

    protected:
        void initializeHelperVectors();
        virtual void simul_step_cuda() = 0;
        float w_random;
        int step = 0;
};
