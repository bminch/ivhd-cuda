#pragma once

#include <utility>
#include "caster/caster_cuda.h"

#define FINALIZING_ITERATIONS 100

class CasterCudaAB : public CasterCuda {
public:
    CasterCudaAB(float w_random): CasterCuda(w_random) {}

    void finalize() override {
        finalizing = true;
        for (int i=0; i<FINALIZING_ITERATIONS; i++) {
            simul_step();
        }
    }

protected:
    void simul_step_cuda() override;

    bool finalizing = false;
};
