#pragma once 

#include <random>
#include <fstream>
#include "config.h"
#include "data.h"
#include "monitor.h"

#define HEAP_LIMIT 1000000000

class Engine {
    public:
        Engine(Data* data, Config* config);
        ~Engine();
        void prepareDataset();
        void setCaster();
        void run();
    private:
        void saveResults(std::vector<float2>& positions);
        void optimize();
        Data* data;
        Config* config;
        Caster* caster;
        Monitor* monitor;
};
