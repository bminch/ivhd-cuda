#pragma once

#include <cuda_runtime.h>

enum DistElemType { etNear, etFar, etRandom, etToRemove };

class DistElem
{
public:
    long i, j;
    float r;
    DistElemType type;

    DistElem() = default;
    DistElem(long pi, long pj) : i(pi), j(pj), r(0), type(etNear){};
    DistElem(long pi, long pj, DistElemType ptype, float pr)
      : i(pi), j(pj), r(pr), type(ptype){};
};

class Neighbors
{
public:
    Neighbors() : i(0), j(0), r(0), type(DistElemType::etNear) {};
    size_t i, j;
    float r;
    DistElemType type;

    bool operator== (Neighbors& rhs) const
    {
        return i == rhs.i && j == rhs.j && r == rhs.r && type == rhs.type;
    }
};
