#pragma once

#include "distance_container.h"
#include "config.h"
#include "string"
#include "vector"

using namespace std;

class Data
{
public:
    Data(Config* config): config(config) {};
    vector<vector<int>> data;
    vector<int> labels;

    static void generateNearestDistances(IDistanceContainer& dstContainer, const string& file);
    static void generateRandomDistances(IDistanceContainer& dstContainer, int n, unsigned rn);

    void load();
    int size() const { return particleCount; }
    vector<int>& labelsRef() { return labels; }
private:
    int particleCount {0};
    Config* config;
};
