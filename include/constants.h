#pragma once

#include <iostream>

using namespace std;

#define EPS 1.19e-07f

#define a_factor 0.9f
#define b_factor 0.002f

inline bool cudaCheck(cudaError_t ret, const char *fileName,
                      unsigned int lineNo) {
  if (ret != cudaSuccess) {
    std::cerr << "CUDA error in " << fileName << ":" << lineNo << std::endl
              << "\t (" << cudaGetErrorString(ret) << ")" << std::endl;
    exit(-1);
  }

  return ret != cudaSuccess;
}

#define cuCall(err) cudaCheck(err, __FILE__, __LINE__)
