#pragma once 

#include <iostream>
#include "caster/caster_cuda_ab.h"
#include "caster/caster_cuda_adadelta.h"
#include "caster/caster_cuda_adam.h"
#include "caster/caster_cuda_nesterov.h"
#include "caster/caster_type.h"

using namespace std;

class Config
{
private:
    CasterType parseCasterType(string algorithm_name);
public:
    Config(int argc, char** argv);

    string dataset_file;
    string labels_file;
    string knn_file;
    string results_dir;
    string experiment_name;
    CasterType caster_type;

    bool verbose;
    float w_random;
    unsigned iterations;
    unsigned seed;
    unsigned random_neighbours;

    int record_error_iterations_period = 100;
    int record_positions_iterations_period = 2000;
};