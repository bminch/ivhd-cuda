#pragma once 

#include <functional>
#include <string>

class KNNParser
{
public:
    static void parseFile(const std::string& fileName, const std::function<void(int, int)>& handlePair);
};
