#pragma once

#include <cuda.h>
#include "Clock.h"
#include <chrono>
#include <vector>
#include <cmath>
#include <fstream>
#include "config.h"
#include "data.h"

class Monitor
{
public:
    Monitor() {}
    virtual ~Monitor() {};
    virtual void startTimer() = 0;
    virtual void recordError(float err) = 0;
    virtual void recordPositions(std::vector<float2>& positions) = 0;
    virtual void printSummary() = 0;
};


class DummyMonitor : public Monitor
{
public:
    DummyMonitor();
    ~DummyMonitor() override {}
    void startTimer() override {}
    void recordError(float err) override;
    void recordPositions(std::vector<float2>& positions) override {}
    void printSummary() override;
private:
    float minError;
};


class PerformanceMonitor : public Monitor
{
private:
    long start;
    long offset;
    float minError;
    std::ofstream errFile;
    const std::vector<int>* labels;
    Config* config;
public:
    PerformanceMonitor(const std::vector<int>* labels, Config* config);
    ~PerformanceMonitor() override;

    void startTimer() override;
    void recordError(float err) override;
    void recordPositions(std::vector<float2>& positions) override;
    void printSummary() override;
};
